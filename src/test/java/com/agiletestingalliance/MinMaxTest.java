package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

  @Test
  public void test_compare() {
	  MinMax obj = new MinMax();
	  int res = new MinMax().valueF(5, 2);
     assertEquals("A bigger than B", 5, res);
  }

  @Test
  public void test_compare2() {
	  MinMax obj = new MinMax();
	  int res = new MinMax().valueF(1, 2);
     assertEquals("B bigger than A", 2, res);
  }

} 
